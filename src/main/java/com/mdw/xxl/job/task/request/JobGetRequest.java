package com.mdw.xxl.job.task.request;

import com.mdw.xxl.job.task.response.ExecutorGetResponse;
import com.mdw.xxl.job.task.response.JobGetResponse;
import lombok.Data;


/**
 * 分页查询执行器
 *
 * @author madingwen
 * @version $Id: ExecutorGetRequest.java, v 0.1 2022-05-03 23:37 mdw Exp $$
 */
@Data
public class JobGetRequest extends BaseXxlJobRequest<JobGetResponse> {

    private String jobGroup;
    private String executorHandler;
    private String jobDesc;
    private String author;
    private String triggerStatus = "-1";
    private Integer start=0;
    private Integer length = 10;

    /**
     * 请求路径
     *
     * @return
     */
    @Override
    public String getUrl() {
        return "/jobinfo/pageList";
    }

    /**
     * 返回结果的类型
     *
     * @return
     */
    @Override
    public Class<JobGetResponse> getResponseClass() {
        return JobGetResponse.class;
    }
}