package com.mdw.xxl.job.task.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author madingwen
 * @version $Id: BaseXxlJobResponse.java, v 0.1 2022-05-05 16:34 mdw Exp $$
 */
@Data
public class BaseXxlJobResponse<T> implements Serializable {
    public static final long serialVersionUID = 1L;

    public static final Integer SUCCESS_CODE = 200;

    private Integer code;
    private String msg;
    private T content;

    public boolean isOk() {
        return SUCCESS_CODE.equals(code);
    }
}