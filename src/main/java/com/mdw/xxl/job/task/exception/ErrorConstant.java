package com.mdw.xxl.job.task.exception;

import lombok.Getter;

/**
 * 错误信息
 *
 * @author madingwen
 * @version $Id: ErrorConstant.java, v 0.1 2022-04-29 11:15 feigeswjtu.cyf Exp $$
 */
@Getter
public class ErrorConstant {

    public static final String INVOKE_XXL_GET_COOKIE_FAILED = "获取Cookie失败";
    public static final String INVOKE_XXL_ADD_EXECUTOR_FAILED = "添加执行器失败";
    public static final String INVOKE_XXL_GET_EXECUTOR_FAILED = "获取执行器失败";
    public static final String INVOKE_XXL_ADD_TASK_FAILED = "添加任务失败";
    public static final String XXL_GET_EXECUTOR_EMPTY = "执行器不存在";
    public static final String PARAM_MISS = "缺少请求参数{%s}";
}