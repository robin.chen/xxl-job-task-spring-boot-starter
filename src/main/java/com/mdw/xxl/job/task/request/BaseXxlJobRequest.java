package com.mdw.xxl.job.task.request;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.Map;

/**
 * xxl-job后台API抽象请求类
 *
 * @author madingwen
 * @version $Id: BaseXxlJobRequest.java, v 0.1 2022-05-03 23:37 mdw Exp $$
 */
public abstract class BaseXxlJobRequest<T> {

    /**
     * 请求路径
     *
     * @return
     */
    public abstract String getUrl();

    /**
     * 返回结果的类型
     *
     * @return
     */
    public abstract Class<T> getResponseClass();

    /**
     * 对象转换为Map
     *
     * @return
     */
    public Map<String, Object> toParamMap() {
        TypeReference<Map<String, Object>> typeReference = new TypeReference<Map<String, Object>>() {
        };
        return JSON.parseObject(JSON.toJSONString(this), typeReference);
    }

    /**
     * 参数校验
     */
    public void check() {
    }
}