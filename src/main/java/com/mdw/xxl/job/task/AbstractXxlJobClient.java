package com.mdw.xxl.job.task;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSON;
import com.mdw.xxl.job.task.config.XxlJobProperties;
import com.mdw.xxl.job.task.exception.BizException;
import com.mdw.xxl.job.task.exception.ErrorConstant;
import com.mdw.xxl.job.task.request.BaseXxlJobApiRequest;
import com.mdw.xxl.job.task.request.BaseXxlJobRequest;
import com.mdw.xxl.job.task.request.CookieGetRequest;
import com.mdw.xxl.job.task.response.BaseXxlJobResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.List;


/**
 * @author madingwen
 * @version $Id: AbstractXxlJobClient.java, v 0.1 2022-05-05 16:24 mdw Exp $$
 */
@Slf4j
public abstract class AbstractXxlJobClient implements XxlJobClient {

    @Autowired
    private XxlJobProperties xxlJobProperties;

    /**
     * 有些接口的返回不一样，没有code，坑爹，特殊处理一下吧
     * 例：比如/jobgroup/pageList，直接用的Map返回
     */
    private static final List<String> UNCHECK_RESULT_API = new ArrayList<>();
    static {
        UNCHECK_RESULT_API.add("/jobgroup/pageList");
        UNCHECK_RESULT_API.add("/jobinfo/pageList");
    }

    @Override
    public <T> T execute(BaseXxlJobRequest<T> request) throws BizException {
        request.check();

        String uri = request.getUrl();
        log.info("请求xxl-job api,url：{},req：{}", uri, JSON.toJSON(request));
        String resultBody = HttpRequest.post(xxlJobProperties.getAdminAddresses() + uri)
                .form(request.toParamMap())
                .cookie(getCookie())
                .execute()
                .body();
        log.info("xxl-job api响应,url：{},result：{}", uri, resultBody);

        if (UNCHECK_RESULT_API.contains(uri)) {
            return JSON.parseObject(resultBody, request.getResponseClass());
        }

        BaseXxlJobResponse<T> response = JSON.parseObject(resultBody, BaseXxlJobResponse.class);
        if (!response.isOk()) {
            log.error("xxl-job api[{}]执行失败,result：{}", uri, JSON.toJSON(response));
            throw new BizException(response.getMsg());
        }
        return response.getContent();
    }

    /**
     * 模拟登录，获取cookie
     *
     * @return
     */
    private String getCookie() {
        CookieGetRequest request = new CookieGetRequest();
        request.setUserName(xxlJobProperties.getLoginUserName());
        request.setPassword(xxlJobProperties.getLoginPwd());

        log.debug("获取xxl cookie,req：{}", JSON.toJSONString(request));
        HttpResponse response = HttpRequest.post(xxlJobProperties.getAdminAddresses() + request.getUrl())
                .form(request.toParamMap())
                .execute();
        if (!response.isOk()) {
            log.error("调用xxl获取cookie失败,response：{}", JSON.toJSONString(response.body()));
            throw new BizException(ErrorConstant.INVOKE_XXL_GET_COOKIE_FAILED);
        }

        List<HttpCookie> cookies = response.getCookies();
        log.debug("获取xxl cookie成功,cookies：{}", JSON.toJSONString(cookies));

        StringBuilder sb = new StringBuilder();
        cookies.stream().forEach(cookie -> sb.append(cookie.toString()));
        return sb.toString();
    }

    /**
     * 执行开放API的请求
     *
     * @param request
     * @return
     */
    @Override
    public <T> T execute(BaseXxlJobApiRequest<T> request) {
        request.check();

        String uri = request.getUrl();
        log.info("请求xxl-job 开放api,url：{},req：{}", uri, JSON.toJSON(request));
        String resultBody = HttpRequest.post(xxlJobProperties.getAdminAddresses() + uri)
                .body(JSON.toJSONString(request))
                .header("XXL-JOB-ACCESS-TOKEN", xxlJobProperties.getAccessToken())
                .execute()
                .body();
        log.info("xxl-job 开放api响应,url：{},result：{}", uri, resultBody);

        BaseXxlJobResponse<T> response = JSON.parseObject(resultBody, BaseXxlJobResponse.class);
        if (!response.isOk()) {
            log.error("xxl-job 开放api[{}]执行失败,result：{}", uri, JSON.toJSON(response));
            throw new BizException(response.getMsg());
        }
        return response.getContent();
    }
}