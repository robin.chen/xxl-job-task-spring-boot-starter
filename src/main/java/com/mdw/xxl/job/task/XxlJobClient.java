package com.mdw.xxl.job.task;

import com.mdw.xxl.job.task.exception.BizException;
import com.mdw.xxl.job.task.request.BaseXxlJobApiRequest;
import com.mdw.xxl.job.task.request.BaseXxlJobRequest;

/**
 * 自动进行模拟登录，调用xxl-job-admin的接口
 *
 * @author madingwen
 * @version $Id: XxlJobClient.java, v 0.1 2022-05-05 15:57 mdw Exp $$
 */
public interface XxlJobClient {

    /**
     * 执行普通请求
     *
     * @param request
     * @param <T>
     * @return
     */
    <T> T execute(BaseXxlJobRequest<T> request) throws BizException;

    /**
     * 执行开放API的请求
     *
     * @param request
     * @param <T>
     * @return
     */
    <T> T execute(BaseXxlJobApiRequest<T> request);
}