package com.mdw.xxl.job.task.exception;

/**
 * @author madingwen
 * @version $Id: BizException.java, v 0.1 2022-04-29 11:12 mdw Exp $$
 */
public class BizException extends RuntimeException {


    public BizException(String message) {
        super(message);
    }
}