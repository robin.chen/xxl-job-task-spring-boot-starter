package com.mdw.xxl.job.task.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author madingwen
 * @version $Id: XxlJobProperties.java, v 0.1 2022-04-29 10:59 mdw Exp $$
 */
@Data
@ConfigurationProperties(prefix = "xxl")
public class XxlJobProperties {

    private String adminAddresses;
    private String accessToken;
    private String appname;
    private String address;
    private String ip;
    private int port;
    private String logPath;
    private int logRetentionDays;

    private boolean autoRegister;
    private String alarmEmail;


    private String loginUserName = "admin";
    private String loginPwd = "123456";
}
