package com.mdw.xxl.job.task.request;

import com.mdw.xxl.job.task.response.ExecutorGetResponse;
import lombok.Data;


/**
 * 分页查询执行器
 *
 * @author madingwen
 * @version $Id: ExecutorGetRequest.java, v 0.1 2022-05-03 23:37 mdw Exp $$
 */
@Data
public class ExecutorGetRequest extends BaseXxlJobRequest<ExecutorGetResponse> {

    private String appname;
    private Integer start=0;
    private Integer length = 10;

    /**
     * 请求路径
     *
     * @return
     */
    @Override
    public String getUrl() {
        return "/jobgroup/pageList";
    }

    /**
     * 返回结果的类型
     *
     * @return
     */
    @Override
    public Class<ExecutorGetResponse> getResponseClass() {
        return ExecutorGetResponse.class;
    }
}