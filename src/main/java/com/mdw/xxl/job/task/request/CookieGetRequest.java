package com.mdw.xxl.job.task.request;

import lombok.Data;

/**
 * 模拟登录，获取Cookie
 *
 * @author madingwen
 * @version $Id: CookieGetRequest.java, v 0.1 2022-05-06 9:24 mdw Exp $$
 */
@Data
public class CookieGetRequest extends BaseXxlJobRequest<String> {

    private String userName;
    private String password;

    /**
     * 请求路径
     *
     * @return
     */
    @Override
    public String getUrl() {
        return "/login";
    }

    /**
     * 返回结果的类型
     *
     * @return
     */
    @Override
    public Class<String> getResponseClass() {
        return String.class;
    }
}