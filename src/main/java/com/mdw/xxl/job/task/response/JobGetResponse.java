package com.mdw.xxl.job.task.response;

import lombok.Data;

import java.util.List;

/**
 * @author madingwen
 * @version $Id: JobUpdateRequest.java, v 0.1 2022-05-05 18:49 mdw Exp $$
 */
@Data
public class JobGetResponse {

    /**
     * 过滤后的总记录数
     */
    private int recordsFiltered;
    /**
     * 总记录数
     */
    private int recordsTotal;
    /**
     * 分页列表
     */
    private List<XxlJobGroupVo> data;

    @Data
    public static class XxlJobGroupVo {

        /**
         * 任务ID
         */
        private int id;
        /**
         * 执行器ID
         */
        private String jobGroup;
        /**
         * 执行器方法
         */
        private String executorHandler;
    }
}