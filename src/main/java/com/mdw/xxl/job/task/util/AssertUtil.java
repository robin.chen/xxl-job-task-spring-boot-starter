package com.mdw.xxl.job.task.util;

import com.mdw.xxl.job.task.exception.BizException;
import com.mdw.xxl.job.task.exception.ErrorConstant;

/**
 * 参数校验工具类
 *
 * @author madingwen
 * @version $Id: AssertUtil.java, v 0.1 2021-07-14 11:44 mdw Exp $$
 */
public class AssertUtil {

    public static void notEmpty(Object value, String fieldName) {
        if (value == null) {
            throw new BizException(String.format(ErrorConstant.PARAM_MISS, fieldName));
        }

        if (value instanceof String) {
            if (((String) value).trim().length() == 0) {
                throw new BizException(String.format(ErrorConstant.PARAM_MISS, fieldName));
            }
        }
    }
}
