package com.mdw.xxl.job.task.response;

import lombok.Data;

import java.util.List;

/**
 * @author madingwen
 * @version $Id: JobUpdateRequest.java, v 0.1 2022-05-05 18:49 mdw Exp $$
 */
@Data
public class ExecutorGetResponse {

    /**
     * 过滤后的总记录数
     */
    private int recordsFiltered;
    /**
     * 总记录数
     */
    private int recordsTotal;
    /**
     * 分页列表
     */
    private List<XxlJobGroupVo> data;

    @Data
    public static class XxlJobGroupVo {

        /**
         * 执行器id
         */
        private int id;
        /**
         * 执行器名称
         */
        private String title;
        /**
         * 执行器AppName
         */
        private String appname;
        private int addressType;
        private String addressList;
        private List<String> registryList;

    }
}