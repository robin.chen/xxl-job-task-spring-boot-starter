package com.mdw.xxl.job.task.request;

import com.mdw.xxl.job.task.util.AssertUtil;
import lombok.Data;

/**
 * xxl-job开放API的抽象请求类
 *
 * @author madingwen
 * @version $Id: BaseXxlJobApiRequest.java, v 0.1 2022-05-03 23:37 mdw Exp $$
 */
@Data
public abstract class BaseXxlJobApiRequest<T> {

    /**
     * accessToken，开放API和普通API不同，普通API需要cookie，开放API需要accessToken
     *
     * @return
     */
    private String accessToken;

    /**
     * 请求路径
     *
     * @return
     */
    public abstract String getUrl();

    /**
     * 返回结果的类型
     *
     * @return
     */
    public abstract Class<T> getResponseClass();


    /**
     * 参数校验
     */
    public void check() {
        AssertUtil.notEmpty(accessToken, "accessToken");
    }
}